﻿using IdentityServer.Data;
using IdentityServer.Models;
using IdentityServer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Quickstart.DataAccess
{
    public class UserDA : IUserRepository
    {
        private ApplicationDbContext _db;

        public UserDA(ApplicationDbContext db)
        {
            _db = db;
        }

        public int AddUser()
        {
            try
            {
                //ApplicationUser user = new ApplicationUser();
                //user.UserName = "bob";
                //user.PasswordHash = "Pass123$";
                //_db.Users.Add(user);
                //_db.SaveChanges();
                return 1;
            }
            catch(Exception ex)
            {
                return 0;
            }
          
        }
    }
}
