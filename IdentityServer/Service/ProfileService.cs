﻿using IdentityModel;
using IdentityServer.Models;
using IdentityServer.Repository;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer.Service
{
    public class ProfileService : IProfileService
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public ProfileService(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            try
            {
                var subjectId = context.Subject.GetSubjectId();
                var user = _userManager.FindByIdAsync(subjectId);

                var claims = new List<Claim>{
            new Claim("FullName", "Test_hmh"),};

                context.IssuedClaims.AddRange(claims);
                return Task.FromResult(0);
            }
            catch (Exception ex)
            {
                return Task.FromResult(0);
            }
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            //>Processing
            var user = _userManager.FindByIdAsync(context.Subject.GetSubjectId());
            context.IsActive = (user != null);

            return Task.FromResult(0);
        }
    }
}
