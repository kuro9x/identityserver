﻿using IdentityServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Repository
{
    public interface IAuthRepository
    {
        object GetUserByIdAsync(string id);
        object GetUserByUsername(string username);
        bool ValidatePassword(string username, string plainTextPassword);
    }
}
