﻿using IdentityServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Repository
{
    public interface IUserRepository
    {
        int AddUser();
    }
}
